// Array Methods
// JS has built-in functions and methods for arrays.
// Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
// push()
// SYNTAX: arrayName.push()

console.log("Current Array:");
console.log(fruits);
let fruitsLength=fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

//pop() - removes
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// unshift()
// add element in the beginning

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift()
// removes element in the beginning
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// splice()
// SYNTAX -> arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
// simultaneously removes element from a specified index number and adds elements

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

// sort()
// re-arranges the array elements in alphanumeric order

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse()
// reverses the array elements in alphanumeric order

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

// Non-Mutator Methods
// unlike mutator methods, non-mutator cannot modify array
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
// Will return the index number of the first matching element
// if no element, returns -1
// Syntax -> arrayName.indexOf(searchValue, fromIndex)
 let firstIndex = countries.indexOf("PH");
 console.log("Result of index method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
 console.log("Result of index method: " + invalidCountry);

// lastIndexOf()
// Returns index number of the last matching element in an array
 let lastIndex = countries.lastIndexOf("PH");
 console.log("Result of index method: " + lastIndex);

 // Getting index number starting from specified index
  let lastIndexStart = countries.lastIndexOf("PH", 4);
 console.log("Result of index method: " + lastIndexStart);

 //slice()
 // slices portions and returns new array
 // SYNTAX: arrayName.slice(startingIndex, endingIndex)

 let slicedArrayA = countries.slice(2);
 console.log("Result from slice method");
 console.log(slicedArrayA);

 let slicedArrayB = countries.slice(2, 4);
 console.log("Result from slice method");
 console.log(slicedArrayB);

// slicing off elements starting from the last element of an array

 let slicedArrayC = countries.slice(-3);
 console.log("Result from slice method");
 console.log(slicedArrayC);

// toString()
// Returns an array as a string separated by commas
// Syntax -> arrayName.toString()

 let stringArray = countries.toString();
 console.log("Result from string method");
 console.log(stringArray);

// concat()
 // SYNTAX arrayA.concat(arrayB)

let taskArray1 = ["drink html", "eat js"];
let taskArray2 = ["inhale css", "breathe sass"];
let taskArray3 = ["get git", "be node"];

  let tasks = taskArray1.concat(taskArray2);
 console.log("Result from concat method");
 console.log(tasks);

// combining multiple arrays
 let allTasks = taskArray1.concat(taskArray2,taskArray3);
 console.log("Result from concat method");
 console.log(allTasks);

// Combining arrays with elements
 let combinedTasks = taskArray1.concat("smell express", "throw react");
 console.log("Result from concat method");
 console.log(combinedTasks);

 // join()
 // Returns an array as a string separated by specified separator string
// Syntax -> arrayName.join('separatorString')
  let users = ["John", "Jane", "Joe", "Robert"];
 console.log(users.join());
 console.log(users.join(''));
 console.log(users.join(' - '));

 // Iteration methods
// forEach()
 // similar to a for loop
 // Syntax -> arrayName.forEach(function(indivElement){statement})

 allTasks.forEach(function(task){
    console.log(task)
});

// Using forEach with conditional statement
let filteredTasks = [];
allTasks.forEach(function(task){
    if(task.length > 10){
        filteredTasks.push(task);
    }
});
console.log("Result of filtered tasks");
console.log(filteredTasks);

// map()
// This is useful for performing tasks where mutating or changing the elements are required.
// SYNTAX let/const resultArray = arrayName.map(function(indivElement))

let number = [1, 2, 3, 4, 5];
let numberMap = number.map(function(number){
    return number * number;
});
// original is not affected
console.log("Original Array");
console.log(number);
console.log("Result of map method");
console.log(numberMap);

// map() vs forEach
let numberForEach = number.forEach(function(){
    return number * number;   
})
// forEach loops over all items in the array but does not return new array
console.log(numberForEach)

// every()
// checks if all element in an array meets the given condition
// Syntax -> let/const resultArray = arrayName.every(function(indivElement){condition})

let allValid = number.every(function(number){
    return (number<3);
})
console.log("Result from every method");
console.log(allValid);


// some() at least 1
let someValid = number.some(function(number){
    return (number<3);
})
console.log("Result from some method");
console.log(someValid);

// filter() returns new array that contains given condition

let filterValid = number.filter(function(number){
    return (number < 3);
})

console.log("Result of filter method");
console.log(filterValid);

// includes()
// checks if the argument passed can be found in array


let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]
let productFound1 = products.includes("Mouse")
console.log("Result of include method");
console.log(productFound1);

let filteredProducts = products.filter(function(product){
    return product.toLowerCase().includes("a");
})
console.log(filteredProducts);

// reduce()
// Evaluates elements from left to right and reduces the array into a single value
// Syntax -> let/const resultArray = arrayName.reduce(function(accumulator, current value){operation})

let iteration = 0;
let reducedArray = number.reduce(function(x, y){
    console.warn("Current Iteration: " + ++iteration)
    console.log("accumulator: " + x);
    console.log("Current Value: " + y);

    // the operation to reduce the array into single value
    return x + y;
})
console.log("Result of reduced method: " + reducedArray);

let list = ["Hello","Again","World"];
let reducedJoin = list.reduce(function(x, y){
    return x + " " + y;
})
console.log("Result of reduce method: " + reducedJoin)